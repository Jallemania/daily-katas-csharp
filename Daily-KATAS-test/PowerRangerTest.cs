
namespace Daily_KATAS_test
{

    public class PowerRangerTest
    {

        [Fact]
        public void Test1()
        {
            //ARRANGE
            double n = 2;
            double a = 49;
            double b = 65;
            double expected = 2;

            //ACT
            double result = PowerRanger.Kata(n, a, b);

            //ASSERT
            Assert.True(result == expected);

        }

        [Fact]
        public void Test2()
        {
            //ARRANGE
            double n = 3;
            double a = 1;
            double b = 27;

            double expected = 3;

            //ACT
            double result = PowerRanger.Kata(n, a, b);

            //ASSERT
            Assert.True(result == expected);

        }

        [Fact]
        public void Test3()
        {
            //ARRANGE
            double n = 10;
            double a = 1;
            double b = 5;

            double expected = 1;

            //ACT
            double result = PowerRanger.Kata(n, a, b);

            //ASSERT
            Assert.True(result == expected);

        }

        [Fact]
        public void Test4()
        {
            //ARRANGE
            double n = 5;
            double a = 31;
            double b = 33;

            double expected = 1;

            //ACT
            double result = PowerRanger.Kata(n, a, b);

            //ASSERT
            Assert.True(result == expected);

        }

        [Fact]
        public void Test5()
        {
            //ARRANGE
            double n = 4;
            double a = 250;
            double b = 1300;

            double expected = 3;

            //ACT
            double result = PowerRanger.Kata(n, a, b);

            //ASSERT
            Assert.True(result == expected);

        }

    }
}