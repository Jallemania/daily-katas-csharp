﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS_test
{
    public class PalindromeDescendantTests
    {
        [Fact]
        public void IsPalindrome()
        {
            PalindromeDescendant palindrome = new();
            int[] trueNums = { 13001120, 23336014, 11 };
            bool[] actual = { false, false, false };

            for (int i = 0; i < 3; i++)
            {
                actual[i] = palindrome.Kata(trueNums[i]);
            }


            Assert.True(actual[0]);
            Assert.True(actual[1]);
            Assert.True(actual[2]);
        }

        [Fact]
        public void IsNotPalindrome()
        {
            PalindromeDescendant palindrome = new();
            int falseNum = 11211230;
            bool actual = false; 
            
            actual = palindrome.Kata(falseNum);
            
            Assert.False(actual);
        }
    }
}
