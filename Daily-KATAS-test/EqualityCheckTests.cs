﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS_test
{
    public class EqualityCheckTests
    {
        [Fact]
        public void Test1()
        {
            EqualityCheck eq = new EqualityCheck();
            int a = 1;
            bool b = true;


            bool result = eq.eqCheck<int, bool>(a, b);

            Assert.False(result);
        }

        [Fact]
        public void Test2()
        {
            EqualityCheck eq = new EqualityCheck();
            int a = 1;
            int b = 1;


            bool result = eq.eqCheck<int, int>(a, b);

            Assert.True(result);
        }

        [Fact]
        public void Test3()
        {
            EqualityCheck eq = new EqualityCheck();
            int a = 0;
            string b = "0";


            bool result = eq.eqCheck<int, string>(a, b);

            Assert.False(result);
        }
    }
}
