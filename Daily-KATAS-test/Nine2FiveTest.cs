﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS_test
{
    public class Nine2FiveTest
    {
        [Fact]
        public void Test1()
        {
            double[] testArr = new double[] { 9, 17, 30, 1.5 };
            string expected = "$240.00";
            Nine2Five nine2five = new Nine2Five();  

            string result = nine2five.Kata( testArr);

            Assert.Equal(expected, result);
        }


        [Fact]
        public void Test2()
        {
            double[] testArr = new double[] { 16, 18, 30, 1.8 };
            string expected = "$84.00";
            Nine2Five nine2five = new Nine2Five();

            string result = nine2five.Kata(testArr);

            Assert.Equal(expected, result);
        }


        [Fact]
        public void Test3()
        {
            double[] testArr = new double[] { 13.25, 15, 30, 1.5 };
            string expected = "$52.50";
            Nine2Five nine2five = new Nine2Five();

            string result = nine2five.Kata(testArr);

            Assert.Equal(expected, result);
        }

    }
}
