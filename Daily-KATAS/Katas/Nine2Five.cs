﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS.Katas
{
    public class Nine2Five
    {
        public string Kata(double[] spec)
        {
            double hoursWorked = spec[1] - spec[0];
            double regularTime = 0;
            double overTime = 0;
            double pay = 0;

            if (spec[1] > 17)
            {
                overTime = spec[1] - 17;
                regularTime = hoursWorked - overTime;
            }
            else
            {
                regularTime = hoursWorked;
            }

            pay = regularTime * spec[2];
            pay += overTime * spec[2] * spec[3];

            pay = Math.Round(pay, 2, MidpointRounding.AwayFromZero);
            string result = "$" + String.Format("{0:0.00}", pay).Replace(",", ".");

            return result;
        }

       
    }
}
