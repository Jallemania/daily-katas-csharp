﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS.Katas
{
    public class PowerRanger
    {
        public static double Kata(double n, double a, double b)
        {
            double pwrOf = 0;
            double iterations = 0;

            for (double i = 1; i <= 10; i++)
            {
                pwrOf = Math.Pow(i, n);

                if (pwrOf >= a && pwrOf <= b)
                {
                    iterations++;
                }
            }

            

            return iterations;
        }
    }
}
