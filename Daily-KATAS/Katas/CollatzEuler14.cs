﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS.Katas
{
    public class CollatzEuler14
    {
        public void LargestIteration()
        {
            BigInteger num = 1;
            BigInteger num2 = 0;
            BigInteger currentCount;
            BigInteger largestCount = 0;

            while (num < 1000000)
            {
                Console.WriteLine($"Currently processing number: {num} Current longest sequence: {largestCount}");

                currentCount = Iterative(num);

                if (currentCount > largestCount)
                {
                    largestCount = currentCount;
                    num2 = num;
                }


                num++;
            }

            Console.Clear();
            Console.WriteLine("The longest sequence was: " + largestCount + ". Produced by the number: " + num2);
        }

        public BigInteger Iterative(BigInteger num)
        {
            BigInteger iterations = 1;

            do
            {
                if (num % 2 == 0)
                {
                    num = num / 2;
                    iterations++;  
                }
                else
                {
                    num = num * 3 + 1;
                    iterations++;
                }

            } while (num != 1);


            return iterations;
        }

    }
}
