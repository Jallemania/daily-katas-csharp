﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_KATAS.Katas
{
    public class PalindromeDescendant
    {
       public bool Kata(int input)
        {
            string inputString = $"{input}";

            string newInputString = AddNumbers(inputString);

            if (newInputString.Length == 2)
            {
                if (newInputString[0] == newInputString[1]) return true;
                else return false;
            }
            else
            {
                string reverseString = ReverseString(newInputString);

                bool isPalindrome = reverseString == newInputString ?  true : false;

                if (isPalindrome) return true;
                else return Kata(int.Parse(newInputString));

            }

            return false;
        }

        private static string AddNumbers(string inputString)
        {
            string newInputString = "";

            for (int i = 0; i < inputString.Length; i += 2)
            {
                int num1 = int.Parse(inputString[i].ToString());
                int num2 = int.Parse(inputString[i + 1].ToString());

                int result = num1 + num2;
                string resultString = result.ToString();

                newInputString += resultString;
            }

            return newInputString;
        }

        private static string ReverseString(string input)
        {
            char[] charArr = input.ToCharArray();
            Array.Reverse(charArr);
            string reverseString = new string(charArr);

            return reverseString;
        }
    }
}
